package usuario.app.apphorrios;

import java.util.ArrayList;

/**
 * Created by juliano on 09/07/15.
 */
public class Curso {
    private String nomeCurso;


    public Curso(String nomeCurso) {
        this.nomeCurso = nomeCurso;

    }

    public void setNomeCurso(String nomeCurso) {
        this.nomeCurso = nomeCurso;
    }


    public String getNomeCurso() {
        return nomeCurso;
    }


}

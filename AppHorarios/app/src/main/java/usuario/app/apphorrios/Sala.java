package usuario.app.apphorrios;

/**
 * Created by juliano on 09/07/15.
 */
public class Sala {
    private int numero;
    private String andar;
    private int predio;

    public Sala(int numero, String andar, String predio) {
        this.numero = numero;
        this.andar = andar;
        this.predio = predio;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public void setAndar(String andar) {
        this.andar = andar;
    }

    public void setPredio(int predio) {
        this.predio = predio;
    }

    public int getNumero() {
        return numero;
    }

    public String getAndar() {
        return andar;
    }

    public int getPredio() {
        return predio;
    }
}

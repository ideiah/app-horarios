package usuario.app.apphorrios;

/**
 * Created by juliano on 09/07/15.
 */
public class Periodo {
    private String horaInicio;
    private String horaFim;
    private String nomeProfessor;
    private String diaDaSemana;

    public Periodo(String horaInicio, String horaFim, String nomeProfessor, String diaDaSemana) {
        this.horaInicio = horaInicio;
        this.horaFim = horaFim;
        this.nomeProfessor = nomeProfessor;
        this.diaDaSemana = diaDaSemana;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public void setNomeProfessor(String nomeProfessor) {
        this.nomeProfessor = nomeProfessor;
    }

    public void setHoraFim(String horaFim) {
        this.horaFim = horaFim;
    }

    public void setDiaDaSemana(String diaDaSemana) {
        this.diaDaSemana = diaDaSemana;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public String getHoraFim() {
        return horaFim;
    }

    public String getNomeProfessor() {
        return nomeProfessor;
    }

    public String getDiaDaSemana() {
        return diaDaSemana;
    }
}

package usuario.app.apphorrios;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.*;
import android.app.*;
import android.widget.*;
import java.util.List;
import java.util.ArrayList;

public class AppHorariosActivity extends ActionBarActivity {
    Button btOk, btCancelar;
    ListView lista_cursos;
    List<String> opcoesCursos;
    ArrayAdapter<String> adaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.seleçaoCurso);

        opcoesCursos = new ArrayList<String>();


        adaptador = new ArrayAdapter<String>(AppHorariosActivity.this, android.R.layout.simple_list_item_1, opcoesCursos);
        lista_cursos.setAdapter(adaptador);

        //carregaTelaLogin();

    }
    public void acionaTelaSelecaoCurso(){
        lista_cursos = (ListView) findViewById(R.id.lista_cursos);
        lista_cursos.setOnClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,

        });
}

/*
        public void carregaTelaLogin(){
            setContentView(R.layout.seleçaoCurso);
            btOk = (Button) findViewById(R.id.btOk);
            btCancelar = (Button) findViewById(R.id.btCancelar);

            btOk.setOnClickListener(new View.OnClickListener() {

              @Override
              public void onClick(View v) {
                  setContentView(R.layout.tela_principal_main);

                  LayoutInflater li = getLayoutInflater();
                  View view = li.inflate(R.layout.tela_proxima_aula, null);

                  AlertDialog.Builder dialogo = new AlertDialog.Builder(AppHorariosActivity.this);

                  dialogo.setTitle("Próxima Aula");
                  dialogo.setView(view);
                  dialogo.setNeutralButton("OK", null);
                  dialogo.show();
              }
              });
        }
*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_app_horarios, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

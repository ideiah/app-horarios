package usuario.app.apphorrios;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by juliano on 13/07/15.
 */
public class Cadastro {
   private List<Curso> cursos;
   private List<Disciplina> disciplinas;
   private List<Periodo> periodos;
   private List<Sala> salas;

    public Cadastro() {
        this.cursos = new ArrayList<Curso>();
        this.disciplinas= new ArrayList<Disciplina>();
        this.periodos = new ArrayList<Periodo>();
        this.salas = new ArrayList<Sala>();

        Sala s1 = new Sala(101, "5", "1º");
        salas.add(s1);
        Sala s3 = new Sala(201, "2", "2º");
        salas.add(s3);
        Sala s4 = new Sala(301, "4", "3º");
        salas.add(s4);

        Periodo p1 = new Periodo("8:30", "10:20", "Amanda", "Segunda");
        periodos.add(p1);
        Periodo p2 = new Periodo("13:30", "15:20", "Ewerson", "Segunda");
        periodos.add(p2);
        Periodo p3 = new Periodo("18:30", "20:20", "Jean", "Segunda");
        periodos.add(p3);
        Periodo p4 = new Periodo("20:30", "22:20", "João", "Segunda");
        periodos.add(p4);
        Periodo p5 = new Periodo("10:30", "12:20", "José", "Segunda");
        periodos.add(p5);

        Disciplina d1 = new Disciplina("IHC – Interação Humano-Computador", "3º");
        disciplinas.add(d1);
        Disciplina d2 = new Disciplina("Lógica Proposicional", "1º");
        disciplinas.add(d2);
        Disciplina d3 = new Disciplina("Algoritmos e Programação", "1º");
        disciplinas.add(d3);
        Disciplina d4 = new Disciplina("Teoria dos Grafos", "2º");
        disciplinas.add(d4);
        Disciplina d5 = new Disciplina("Programação Orientada a Objetos", "2º");
        disciplinas.add(d5);


    }

}

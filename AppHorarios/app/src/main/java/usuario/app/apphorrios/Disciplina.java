package usuario.app.apphorrios;

import java.util.ArrayList;

/**
 * Created by juliano on 09/07/15.
 */
public class Disciplina {
    private String nomeDisciplina;
    private String semestre;


    public Disciplina(String nomeDisciplina, String semestre) {
        this.nomeDisciplina = nomeDisciplina;
        this.semestre = semestre;
    }

    public void setNomeDisciplina(String nomeDisciplina) {
        this.nomeDisciplina = nomeDisciplina;
    }


    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public String getNomeDisciplina() {
        return nomeDisciplina;
    }

    public String getSemestre() {
        return semestre;
    }
}
